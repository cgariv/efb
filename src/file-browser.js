// ***** Global vars ***** //
const { Dirent } = require('fs');
const { read } = require('fs');
const fs = require('fs');
const path = require('path');
var target;
var dpath = ["Z://"];
var dpathstr = dpath.join('');
function read_and_write(h) {
  var dirents = fs.readdirSync(h, { withFileTypes: true });
  var fileNames = dirents.filter(dirent => dirent.isFile()).map(dirent => dirent.name);
  var dirNames = dirents.filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);
  var symNames = dirents.filter(dirent => dirent.isSymbolicLink()).map(dirent => dirent.name);
  var dirl = document.querySelector('#nice'); //element where list items are placed
  document.getElementById("dir-input").value = '' + h + '';
  if (dirNames.length == 0 && fileNames.length == 0 && symNames.length == 0) {
    alert("Empty!")
  }

  // ***** Make table headers ***** //
  let tr = document.createElement('tr');
  tr.innerHTML = '<th> Name </th><th> Date Modified </th><th> Size </th><th> Permissions </th>';
  dirl.appendChild(tr); // appending list items

  // ***** For loop to list the folders in the directory ***** //
  for (let i = 0; i < dirNames.length; i++) {
    try {
      let stat = fs.statSync(h + dirNames[i]);
      mtimestr = stat["mtime"].toString();
      reg= /^(?:[^ ]*[ ]){4}[^ ]*([ ])/;
      rmtime= reg.exec(mtimestr);
      mtime=rmtime[0];
      size = stat["size"];//.toString();
      mode = stat["mode"];//.toString();
    }
    catch (err) {
      dirNames.splice(dirNames[i], 1);
    }
    let tr = document.createElement('tr');
    tr.innerHTML = '<td class ="dir-item" id = "' + dirNames[i] + '"><input type = "checkbox" id="'+dirNames[i]+'"><image class ="dir-item" id = "' + dirNames[i] + '" style = "width:20px; height:20px;"src = "folder-solid.svg"></image><a  class = "dir-item" id = "' + dirNames[i] + '"> ' + dirNames[i] + '</a></td><td>' + mtime + '</td><td>' + size + '</td><td>' + mode + '</td>'; //html of list items
    dirl.appendChild(tr); // appending list items
  }
  // ***** For loop to list the symbolic links in the directory ***** //
  for (let i = 0; i < symNames.length; i++) {
    try {
      let stat = fs.statSync(h + symNames[i]);
      mtimestr = stat["mtime"].toString();
      reg= /^(?:[^ ]*[ ]){4}[^ ]*([ ])/;
      rmtime= reg.exec(mtimestr);
      mtime=rmtime[0];
      size = stat["size"];//.toString();
      mode = stat["mode"];//.toString();
    }
    catch (err) {
      dirNames.splice(synNames[i], 1);
    }
    let tr = document.createElement('tr');
    tr.innerHTML = '<td class ="dir-item" id = "' + symNames[i] + '"><input type = "checkbox" id="'+symNames[i]+'"><image class ="dir-item" id = "' + symNames[i] + '" style = "width:20px; height:20px;"src = "folder-solid.svg"></image><a  class = "dir-item" id = "' + symNames[i] + '"> ' + symNames[i] + '</a></td><td>' + mtime + '</td><td>' + size + '</td><td>' + mode + '</td>'; //html of list items
    dirl.appendChild(tr); // appending list items
  }
  // ***** For loop to list the files in the directory ***** //
  for (let i = 0; i < fileNames.length; i++) {
    try {
      let stat = fs.statSync(h + fileNames[i]);
      mtimestr = stat["mtime"].toString();
      reg= /^(?:[^ ]*[ ]){4}[^ ]*([ ])/;
      rmtime= reg.exec(mtimestr);
      mtime=rmtime[0];
      size = stat["size"];//.toString();
      mode = stat["mode"];//.toString();
    }
    catch (err) {
      dirNames.splice(fileNames[i], 1);
    }
    let tr = document.createElement('tr');
    tr.innerHTML = '<td class ="dir-item" id = "' + fileNames[i] + '"><input type = "checkbox" id="'+fileNames[i]+'"><image class ="dir-item" id = "' + fileNames[i] + '" style = "width:20px; height:20px;"src = "folder-solid.svg"></image><a  class = "dir-item" id = "' + fileNames[i] + '"> ' + fileNames[i] + '</a></td><td>' + mtime + '</td><td>' + size + '</td><td>' + mode + '</td>'; //html of list items
    dirl.appendChild(tr); // appending list items
  }
}
read_and_write(dpathstr);
// ***** Remove listed items ***** //
function rm_dir_items() {
  var list = document.getElementById("nice");  // Get the <ul> element with id="myList"
  while (list.hasChildNodes()) {
    list.removeChild(list.firstChild); // As long as <ul> has a child node, remove it
  }
}
// ***** Event listener for clicks on dir-items ***** //
document.addEventListener('dblclick', function (event) {
  if (!event.target.matches('.dir-item')) return; // check if clicked element matches the class
  target = event.target.id // set target to element id
  dpath.push(event.target.id + "/");
  dpathstr = dpath.join('');
  try {
    if (fs.lstatSync(dpathstr).isDirectory() == true) {
      rm_dir_items();
      read_and_write(dpathstr);
    }
    else {
      alert("Not a dir");
      back();
    }
  }
  catch (Err) {
    alert(Err.message);
    back();
  }
}, false);

function back() {
  var dleg = dpath.length;
  if (dleg > 1) {
    dpath.pop();
    dpathstr = dpath.join('');
  }
}
function bbutton() {
  rm_dir_items();
  back();
  read_and_write(dpathstr);
}
function sub() {
  var val = document.getElementById("dir-input").value;
  rm_dir_items();
  read_and_write(val);
}


//function preview() {
  //document.getElementById("preview-box").innerHTML = '<iframe src="https://www.w3schools.com" title="W3Schools Free Online Web Tutorials"></iframe>';
//}
